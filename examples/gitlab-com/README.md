# Mirroring about.gitlab.com

about.gitlab.com can be built with just a docker container. Pick one of the two jobs to build the nanoc website.

For the sake of retaining relative links, this website will need to be deployed to a subdomain (about.gitlab.com) or top level domain (gitlabdocs.com).
A website like page.gitlab.io/about-gitlab-com will result in various 404s

if you choose to deploy to an S3 bucket, you will need to provide the following CI variables within the mirror:

1. ACCESS_KEY_ID
1. SECRET_KEY
1. BUCKET_NAME