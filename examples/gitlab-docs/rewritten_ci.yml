image: ruby:2.5

stages:
  - build-dependencies
  - build
  - deploy

#
# Pick the remote branch, by default master (see the Rakefile for more info)
#
variables:
  BRANCH_CE: 'master'
  BRANCH_EE: 'master'
  BRANCH_OMNIBUS: 'master'
  BRANCH_RUNNER: 'master'
  BRANCH_CHARTS: 'master'

#
# Check Ruby version and install gems
#
before_script:
  - ruby -v
  - bundle install --jobs 4 --path vendor

# Skip this job when it's invoked by a cross project pipeline. That will speed
# up the pipeline when a docs preview is triggered. The triggered pipeline is
# always a branch off master which should be green anyway. For more info:
# https://docs.gitlab.com/ee/development/writing_documentation.html#previewing-the-changes-live
.except_pipelines: &except_pipelines
  except:
    - pipelines
#
# Except stable branches (with names like '10.4', '11.2', etc.)
#
.except_stable: &except_stable
  except:
    - /^\d{1,2}\.\d{1,2}$/

#
# Retry a job automatically if it fails (2 times)
#
.retry: &retry
  retry: 2

retrieve-dependencies:
  stage: build-dependencies
  before_script:
    - ''
  script:
    - ./build_dependencies.sh
    - if [ ! -d $CI_PROJECT_DIR/gitlab-docs-deps ]; then mkdir $CI_PROJECT_DIR/gitlab-docs-deps; fi
    - if [ ! -d $CI_PROJECT_DIR/gitlab-docs-deps/content ]; then mkdir $CI_PROJECT_DIR/gitlab-docs-deps/content; fi
    - cp -r ~/gitlab-docs-deps/gitlab-ce/doc $CI_PROJECT_DIR/gitlab-docs-deps/content/ce
    - cp -r ~/gitlab-docs-deps/gitlab-ee/doc $CI_PROJECT_DIR/gitlab-docs-deps/content/ee
    - cp -r ~/gitlab-docs-deps/omnibus-gitlab/doc $CI_PROJECT_DIR/gitlab-docs-deps/content/omnibus
    - cp -r ~/gitlab-docs-deps/gitlab-runner/docs $CI_PROJECT_DIR/gitlab-docs-deps/content/runner
    - cp -r ~/gitlab-docs-deps/charts/doc $CI_PROJECT_DIR/gitlab-docs-deps/content/charts
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME-Docs"
    paths:
      - gitlab-docs-deps/
  # Replace with a shell runner of choice
  tags:
    - dmz

###############################################
#             Build the website               #
###############################################

.build_base: &build_base
  stage: build
  script:
    - /bin/bash --login
    - rvm use 2.5.3
    - cp ~/gitlab-docs-deps ./ -r
    - ln -s $CI_PROJECT_DIR/gitlab-docs-deps/gitlab-ce/doc $CI_PROJECT_DIR/content/ce
    - ln -s $CI_PROJECT_DIR/gitlab-docs-deps/gitlab-ee/doc $CI_PROJECT_DIR/content/ee
    - ln -s $CI_PROJECT_DIR/gitlab-docs-deps/omnibus-gitlab/doc $CI_PROJECT_DIR/content/omnibus
    - ln -s $CI_PROJECT_DIR/gitlab-docs-deps/gitlab-runner/docs $CI_PROJECT_DIR/content/runner
    - ln -s $CI_PROJECT_DIR/gitlab-docs-deps/charts/doc $CI_PROJECT_DIR/content/charts
    - ls -al $CI_PROJECT_DIR/content
    - bundle exec nanoc compile -VV
  artifacts:
    paths:
      - public
    expire_in: 1w
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      - tmp/
      - vendor/ruby
  tags:
    # Replace with a shell runner of choice
    - dmz
  <<: *retry

#
# Compile only on master and stable branches
#
compile_prod:
  <<: *build_base
  variables:
    NANOC_ENV: 'production'
  only:
    - master
    - /^\d{1,2}\.\d{1,2}$/

#
# Compile on all branches except master
#
compile_dev:
  <<: *build_base
  only:
    - branches
  except:
    - master
    - /^\d{1,2}\.\d{1,2}$/


###############################################
#          GitLab Pages (production)          #
###############################################

#
# Deploy to production with GitLab Pages
#
pages:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  before_script: []
  cache: {}
  dependencies:
    - compile_prod    # Contains the public directory
  script:
    # Symlink all README.html to index.html so that we can have clean URLs
    - for i in `find public -name README.html`; do ln -sf README.html $(dirname $i)/index.html; done
  artifacts:
    paths:
    - public
    - environment_url.txt
    expire_in: 1d
  only:
    - master
  tags:
    - dmz
  <<: *retry
