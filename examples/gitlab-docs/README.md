# Mirroring docs.gitlab.com

This example contains an additional script to be pushed to the mirror.

docs.gitlab.com relies on various other repositories so it would be best to set up this mirror to 
build off a shell runner to save any time during mirror updates. You could also build a docker image 
containing the various git repos and various dependencies, too.

For the sake of retaining relative links, this website will need to be deployed to a subdomain (about.gitlab.com) or top level domain (gitlabdocs.com).
A website like page.gitlab.io/about-gitlab-com will result in various 404s