#!/bin/bash

# Requires a shell runner to decrease number of clones

pwd=$(pwd)

if [ ! -d ~/gitlab-docs-deps ]; then
    echo "Generating Dependencies"
    mkdir ~/gitlab-docs-deps
    cd ~/gitlab-docs-deps
    git clone git@gitlab.com:gitlab-org/gitlab-ce.git
    git clone git@gitlab.com:gitlab-org/gitlab-ee.git
    git clone git@gitlab.com:gitlab-org/gitlab-runner.git
    git clone git@gitlab.com:gitlab-org/omnibus-gitlab.git
    git clone git@gitlab.com:charts/gitlab.git charts
    cd $pwd
    ln -s $(pwd)/gitlab-docs-deps/gitlab-ce/doc $(pwd)/content/ce
    ln -s $(pwd)/gitlab-docs-deps/gitlab-ee/doc $(pwd)/content/ee
    ln -s $(pwd)/gitlab-docs-deps/omnibus-gitlab/doc $(pwd)/content/omnibus
    ln -s $(pwd)/gitlab-docs-deps/gitlab-runner/docs $(pwd)/content/runner
    ln -s $(pwd)/gitlab-docs-deps/charts/doc $(pwd)/content/charts
else
    echo "Updating dependencies"
    cd gitlab-docs-deps
    for dir in */
    do
        cd $dir
        git pull
        cd ..
    done
fi
