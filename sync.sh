#!/bin/bash

# DMZ Repo Script

# Check for any defined repos
if [ -d repos ]; then
    cd repos
    pwd=$(pwd)

    # Iterate over each repo directory
    for dir in */
    do
        dir=${dir%*/}

        cd $dir
        repo_dir=$(pwd)

        # Generate variables from .repo_details
        source .repo_details

        # Check for existing repo in runner
        if [ ! -d ~/$REPO ]; then 
            git clone $SOURCE_REPO_URL ~/$REPO
        fi

        cd ~/$REPO

        # Reset baseline for code
        git fetch -a
        git reset --hard origin/master

        # Check if new remote has been configured
        for r in $(git remote -vv | awk '{print $1}'); 
        do 
            if [ $r == "new-remote" ]; then 
                export REMOTE_EXISTS=True
            fi
        done

        # Adding new remote if it doesn't exist
        if [ "$REMOTE_EXISTS" != "True" ]; then 
            echo "Adding remote"
            git remote add new-remote $DESTINATION_REPO_URL
        fi

        # Checking for a rewritten_ci file and force pushing it up
        if [ -a $repo_dir/rewritten_ci.yml ]; then
            cp $repo_dir/rewritten_ci.yml ~/$REPO/.gitlab-ci.yml
            git add .gitlab-ci.yml
            # Checking for any additional scripts to assist with the mirror
            myarray=(`find $repo_dir -maxdepth 1 -name "*.sh"`)
            if [ ${#myarray[@]} -gt 0 ]; then
                chmod +x $repo_dir/*.sh 
                cp $repo_dir/*.sh ~/$REPO/
                git add *.sh
            fi
            git commit -m "Syncing repo"
            git push new-remote master -f
        else
            git push new-remote master
        fi

        export REMOTE_EXISTS=False
        cd $pwd

    done

else
    echo "No repo directory found"
fi
    