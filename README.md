# Mirroring repos through a DMZ

Create a folder for a repo containing two files:

- .repo_details (required)
- rewritten_ci.yml (optional)
- any_additional_scripts.sh (optional)

The structure should look like the following:

- repos
  - new repo
    - .repo_details
    - rewritten_ci.yml
    - script1.sh
    - script2.sh

Push these changes to a branch to trigger the CI pipeline. You may have to force add them since the repos directory is ignored by default

## .repo_details structure

```bash
REPO=repo-name
SOURCE_REPO_URL=git@gitlab.com/repo.git
DESTINATION_REPO_URL=git@gitlab.com/repo2.git

```

## rewritten_ci.yml

Completely optional, but if you need to overwrite an existing ci file for a synced project, 
you can provide the new file within the repo directory and the CI job will pick it up and replace the existing CI before pushing to the destination

## any_additional_scripts.sh

Any `.sh` scripts sitting alongside `.repo_details` will be picked up and pushed to the mirror through the DMZ.
This is usually used for providing any additional functionality needed in the rewritten_ci.yml file.

## DMZ runner setup

The DMZ should contain a local copy of each of these repositories to save time updating any changes to the mirror.
This was created under the assumption that the DMZ will act as a shell runner.